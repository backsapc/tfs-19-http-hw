package tfs.scala.http.client

import org.json4s.DefaultFormats
import org.json4s.jackson.Serialization._
import org.scalatest.{FlatSpecLike, Matchers}

case class JsonExample(foo: String, bar: Seq[String], bazz: Option[Int])

class JsonTest extends FlatSpecLike with Matchers {
  implicit val formats = DefaultFormats

  val example = JsonExample("Foo", Seq("B", "A", "R"), Some(1))
  val json = """{"foo":"Foo","bar":["B","A","R"],"bazz":1}"""

  behavior of "Json4s"

  it should "parse json" in {
    read[JsonExample](json) shouldBe example
  }

  it should "serialize to json" in {
    write(example) shouldBe json
  }
}